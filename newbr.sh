#!/bin/bash

if [ $# -ne 2 ]; then
    echo "$0 <br-name> <text>"
    exit 1
fi
git pull
git reset --hard
git checkout master
git pull
git checkout -b $1
echo $2 >> README.en.md
git commit -a -m "changes for $1"
git push --set-upstream origin $1
git checkout master
git pull
